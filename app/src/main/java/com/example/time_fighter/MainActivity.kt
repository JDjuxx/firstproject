package com.example.time_fighter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var titleMessageTextView : TextView
    private lateinit var subtitleMessageTextView : TextView
    private lateinit var changeTextButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        titleMessageTextView = findViewById(R.id.message_title)
        subtitleMessageTextView = findViewById(R.id.sub_title_message)
        changeTextButton = findViewById(R.id.change_text_button)

        changeTextButton.setOnClickListener{ changeTitleAndSubTitle()}
    }


    private fun changeTitleAndSubTitle(){
        titleMessageTextView.text = getString(R.string.new_title)
        subtitleMessageTextView.text = getString(R.string.new_subtitle)
    }
}
